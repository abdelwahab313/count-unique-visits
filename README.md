# Coding Challenge Solution

This is a solution to the code challenge described [below](#coding-challenge-developer)
###### Dependencies
* JAVA 8
* Maven 3.6.1

### To Run the project
* To build the project run `mvn package` this will generate a Jar at `target/FlightRightCodeChallenge-1.0-SNAPSHOT.jar`
* To parse a visits file you need to run this command ` java -jar target/FlightRightCodeChallenge-1.0-SNAPSHOT.jar  <path to file>`
* To run The tests you can run `mvn test`


# Coding Challenge Developer:

Write a Java application, that counts users visited our web-page from different sources.  
Mostly filled with duplicates. 
A unique user is identified via unique phone and email combination.

As input you have a csv file, that contains:

```
email,phone,source
test@test.com,123,google.com
test@test.com,,google.com
test1@test.com,321,google.com
```

## The rules:
* The input file can be really huge (gigabytes)
* Ignore csv entries with any nullable field
* Just print the results to any output
* The application is not a single-use script, so should be designed to be supportable
* The filename should be passed in any convenient way, but should not be hardcoded.

# HINT
## Keep in mind:
* Agile manifesto
* Clean code
* Software craftsmanship