package com.flightRight.countVisits;

import java.util.Objects;

public class Visitor {
    private String email;
    private String phoneNumber;
    private String source;

    Visitor(String visitorEmail, String visitorPhoneNumber, String visitorSource) {
        this.email = visitorEmail;
        this.phoneNumber = visitorPhoneNumber;
        this.source = visitorSource;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public String getSource() {
        return this.source;
    }


    public int hashCode() {
        return Objects.hash(this.email, this.phoneNumber);
    }

    public boolean equals(Object o) {
        if (this == o) return true;
        Visitor visitor = (Visitor) o;
        return (this.email.equals(visitor.email) && this.phoneNumber.equals(visitor.phoneNumber));
    }
}
