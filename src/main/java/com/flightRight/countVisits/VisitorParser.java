package com.flightRight.countVisits;

import java.util.Arrays;

public class VisitorParser {
    private Visitor visitor = null;
    private Boolean isValid;

    VisitorParser(String rawLine) {
        parse(rawLine);
    }

    private void parse(String rawLine) {
        String[] tokens = rawLine.split(",");
        tokens = Arrays.stream(tokens).filter(s -> !s.trim().isEmpty()).toArray(String[]::new);
        if (tokens.length == 3) {
            this.visitor = new Visitor(tokens[0], tokens[1], tokens[2]);
            this.isValid =  true;
        } else {
            this.isValid =  false;
        }
    }

    public boolean isValidLine() {
        return this.isValid;
    }

    public Visitor getParsedVisitor() {
        return this.visitor;
    }
}
