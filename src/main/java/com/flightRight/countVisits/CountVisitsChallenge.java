package com.flightRight.countVisits;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;

public class CountVisitsChallenge {
    public static void main(String[] args) {
        File file = new File(getPathToTargetFile(args));
        int uniqueVisitsCount = calculateUniqueVisits(file);
        System.out.println("Total unique users count is " + uniqueVisitsCount);
    }

    public static int calculateUniqueVisits(File file) {
        HashSet<Visitor> totalVisitors = new HashSet<Visitor>();

        try (BufferedReader b = new BufferedReader(new FileReader(file))) {
            String readLine = "";
            String headerLine = b.readLine();

            System.out.println("Reading file ...");
            while ((readLine = b.readLine()) != null) {
                VisitorParser visitorParser = new VisitorParser(readLine);
                Boolean isValid = visitorParser.isValidLine();
                if (isValid) {
                    Visitor visitor = visitorParser.getParsedVisitor();
                    totalVisitors.add(visitor);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return totalVisitors.size();
    }

    static String getPathToTargetFile(String[] args) {
        if (args.length >= 1) {
            return args[0];
        }
        return "src/main/resources/config/testData.txt";
    }

}
