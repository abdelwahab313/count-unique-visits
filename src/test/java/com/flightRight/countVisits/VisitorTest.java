package com.flightRight.countVisits;

import org.junit.Test;


import java.util.HashSet;

import static org.junit.Assert.assertEquals;

public class VisitorTest {

    @Test
    public void visitorWithSamePhoneDifferentEmail() {
        HashSet<Visitor> totalVisitors = new HashSet<Visitor>();

        Visitor visitor1 = new Visitor("abc@google.com", "0123", "google.com");
        Visitor visitor2 = new Visitor("a@google.com", "0123", "google.com");

        totalVisitors.add(visitor1);
        totalVisitors.add(visitor2);
        assertEquals(totalVisitors.size(), 2);
    }

    @Test
    public void visitorWithSameEmailDifferentPhone() {
        HashSet<Visitor> totalVisitors = new HashSet<Visitor>();

        Visitor visitor1 = new Visitor("abc@google.com", "0123", "google.com");
        Visitor visitor2 = new Visitor("abc@google.com", "0125", "google.com");

        totalVisitors.add(visitor1);
        totalVisitors.add(visitor2);
        assertEquals(totalVisitors.size(), 2);
    }

    @Test
    public void visitorWithSameEmailAndPhone() {
        HashSet<Visitor> totalVisitors = new HashSet<Visitor>();

        Visitor visitor1 = new Visitor("abc@google.com", "0123", "google.com");
        Visitor visitor2 = new Visitor("abc@google.com", "0123", "google.com");

        totalVisitors.add(visitor1);
        totalVisitors.add(visitor2);
        assertEquals(totalVisitors.size(), 1);
    }
}
