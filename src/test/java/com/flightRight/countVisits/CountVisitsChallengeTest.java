package com.flightRight.countVisits;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;



public class CountVisitsChallengeTest {

    @Test
    public void testParsedFile() {
        File f = new File("src/main/resources/config/testData.txt");

        int count = CountVisitsChallenge.calculateUniqueVisits(f);
        assertEquals(count, 3);
    }
}
