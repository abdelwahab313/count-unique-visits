package com.flightRight.countVisits;


import org.junit.Test;

import static org.junit.Assert.*;

public class VisitorParserTest {

    @Test
    public void testParseValidLine() {
        VisitorParser visitorParser = new VisitorParser("test1@test.com,321,google.com");
        Boolean isValidLine = visitorParser.isValidLine();
        assertEquals(isValidLine, true);
    }

    @Test
    public void testParseInValidLineWithTwoFields() {
        VisitorParser visitorParser = new VisitorParser("test1@test.com,google.com");
        Boolean isValidLine = visitorParser.isValidLine();
        assertEquals(isValidLine, false);
    }

    @Test
    public void testParseInValidLineWithEmptyPhoneNumber() {
        VisitorParser visitorParser = new VisitorParser("test1@test.com,,google.com");
        Boolean isValidLine = visitorParser.isValidLine();
        assertEquals(isValidLine, false);
    }

    @Test
    public void testParseInValidLineWithSpacesInPhoneNumberField() {
        VisitorParser visitorParser = new VisitorParser("test1@test.com,          ,google.com");
        Boolean isValidLine = visitorParser.isValidLine();
        assertEquals(isValidLine, false);
    }

    @Test
    public void testParseInValidLineReturnsNullVisitor() {
        VisitorParser visitorParser = new VisitorParser("test1@test.com,,google.com");
        Visitor parsedVisitor = visitorParser.getParsedVisitor();
        assertNull(parsedVisitor);
    }

    @Test
    public void testParseVisitorObject() {
        VisitorParser visitorParser = new VisitorParser("test1@test.com,321,google.com");
        Visitor parsedVisitor = visitorParser.getParsedVisitor();
        assertNotNull(parsedVisitor);
        assertEquals(parsedVisitor.getEmail(), "test1@test.com");
        assertEquals(parsedVisitor.getPhoneNumber(), "321");
        assertEquals(parsedVisitor.getSource(), "google.com");
    }
}